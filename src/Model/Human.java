package Model;

import java.util.ListIterator;

/**
 * Classe Human
 * Humain basique qui ne peut que fuire
 * @author Home
 * 
 */
public class Human extends Perso {
	protected int _dhuman;
	protected int _attackh;
	protected int _infection;
	protected boolean _infected;

	/**
	 * @param x
	 * @param y
	 */
	public Human(int x, int y) {
		super(x, y);
		this._persoSize = Constants.getInstance().getHumanSize();
		this._dhuman = 1;
		this._attackh = 0;
		this._ra = 0;
		this._rv = 35;
		this._hp = 100;
		this._infection = 0;
		this._infected = false;
		this._dx = this._dy = _dhuman;
		_typeEnemy = "Zombie";
		_type = "Human";
	}

	public Human(Entity e) {
		this(e.getX(), e.getY());
	}

	@Override
	public void speak() {
		super.speak();
	}

	/**
	 * fonction qui va permettre aux humains normaux de fuir 1) tous les humains
	 * calculent leur vecteurs vitesse 2) tous les humains fuient
	 */
	public void move() {
		if (!_enemies.isEmpty())
			this.escape1();
		else
			super.move();
		// Collision pour les murs
		super.WallCollision();
		
		this.affectation();
		this.effect();
	}

	/**
	 * strategie de fuite numero 1
	 */
	private void escape1() {
		// a mieux init
		double d, min = 10000;
		int px, py, gx, gy;
		ListEntity zombieproche = new ListEntity();
		px = getX();
		py = getY();
		Entity e = null;

		for (ListIterator<Entity> i = _enemies.listIterator(); i.hasNext();) {
			e = i.next();
			_dx = 1;
			_dy = 1;
			gx = e.getX();
			gy = e.getY();
			d = max(Math.abs(gx - px), Math.abs(gy - py));
			// Calcul de la plus proche menace
			if (d < min) {
				min = d;
				// On corrige le vecteur vitesse
				if (gx < px)
					_dx = 1;
				if (gx > px)
					_dx = -1;
				if (gx == px)
					_dx = 0;

				if (gy < py)
					_dy = 1;
				if (gy > py)
					_dy = -1;
				if (gy == py)
					_dy = 0;
			}
		}
		zombieproche.add(e);
		// for(ListEntity.ListEntityIterator i =
		// Game.getInstance().getListEntity().createIterator(); !i.isDone();
		// i.next()){
		// gx = i.current().getX();
		// gy = i.current().getY();
		// if(px+_dx==gx && py+_dy==gy){
		// _dx = 0;
		// _dy = 0;
		// }
		// }

		// Apres le calculde dx et dy on regarde dans les zombie proche ou on
		// peut aller.....
		for (ListIterator<Entity> i = zombieproche.listIterator(); i.hasNext();) {
			e = i.next();
			gx = e.getX();
			gy = e.getY();
			if (px + _dx == gx && py + _dy == gy) {
				_dx = 0;
				_dy = 0;
			}
		}

		// Et aussi dans les humains proches, pour eviter la collision
		for (ListIterator<Entity> i = _friends.listIterator(); i.hasNext();) {
			e = i.next();
			gx = e.getX();
			gy = e.getY();
			if (px + _dx == gx && py + _dy == gy) {
				_dx = 0;
				_dy = 0;
			}
		}
		zombieproche = null;
	}

	private void affectation() {
		_x += (_dx) * _dhuman;
		_y += (_dy) * _dhuman;
	}

	@Override
	public void action() {
		super.action();
	}

	@Override
	void effect() {
		if(_infected)
			_infection++;
	}

	/**
	 * @return
	 */
	public int getInfection() {
		return _infection;
	}

	public void setInfected(boolean infected) {
		this._infected = infected;
	}
	
	public boolean isInfected() {
		return _infected;
	}
}
