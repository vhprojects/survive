package Model;

/**
 * Classe abstraite qui les parrametres de base
 * @author home
 *
 */
public abstract class Entity {
	protected int _hp;
	protected int _x;
	protected int _y;
	protected int _dx;
	protected int _dy;
	protected int _persoSize;

	protected Entity() {
	}

	public void setDx(int dx) {
		this._dx = dx;
	}

	public void setDy(int dy) {
		this._dy = dy;
	}

	public Entity(int x, int y) {
		_x = x;
		_y = y;
	}

	public int getHp() {
		return _hp;
	}

	public void setHp(int _hp) {
		this._hp = _hp;
	}

	public int getY() {
		return _y;
	}

	public int getX() {
		return _x;
	}

	public void setY(int y) {
		this._y = y;
	}

	public void setX(int x) {
		this._x = x;
	}

	public void setPosition(int x, int y) {
		this._x = x;
		this._y = y;
	}

	abstract public void check();

	abstract public void speak();

	abstract public void move();

	abstract public void action();
}
