package Model;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ListIterator;

/**
 * Classe Zombie qui tue les humains 
 * @author Home
 * 
 */
public class Zombie extends Perso {
	private final int _dzombie = 2;
	private final int _attackz = 5;

	protected Zombie() {
	}

	/**
	 * @param x
	 * @param y
	 */
	public Zombie(int x, int y) {
		super(x, y);
		_persoSize = Constants.getInstance().getZombieSize();
		this._ra = 20;
		this._rv = 60;
		this._hp = 100;
		this._dx = this._dy = _dzombie;
		_typeEnemy = "Human";
		_type = "Zombie";
	}
	
	public Zombie(Entity e) {
		this(e.getX(), e.getY());
	}

	@Override
	public void check() {
		super.check();
	}

	@Override
	public void speak() {
		super.speak();
	}

	public void move() {
		if (!_enemies.isEmpty())
			hunt1();
		else
			super.move();
		super.WallCollision();
		affectation();
	}

	/**
	 * Strategie de chasse 1
	 */
	private void hunt1() {
		// a mieux init
		double d, min = Constants.getInstance().getGAMEPANEL_SIZE().width;
		int px, py, gx, gy;
		px = getX();
		py = getY();

		for (ListIterator<Entity> i = _enemies.listIterator(); i.hasNext();) {
			Entity e = i.next();
			_dx = 1;
			_dy = 1;
			gx = e.getX();
			gy = e.getY();
			d = max(Math.abs(gx - px), Math.abs(gy - py));

			// Calcul de la plus proche cible sans prendre ses coordonnés
			if (d < min) {
				min = d;
				if (gx - px < 0)
					_dx = -1;
				if (gx - px > 0)
					_dx = 1;
				if (gx == px)
					_dx = 0;

				if (gy - py < 0)
					_dy = -1;
				if (gy - py > 0)
					_dy = 1;
				if (gy == py)
					_dy = 0;
			}
		}

		for (ListIterator<Entity> i = _friends.listIterator(); i.hasNext();) {
			Entity e = i.next();
			gx = e.getX();
			gy = e.getY();
			if (px + _dx == gx && py + _dy == gy) {
				_dx = 0;
				_dy = 0;
			}
		}

		for (ListIterator<Entity> i = _enemies.listIterator(); i.hasNext();) {
			Entity e = i.next();
			gx = e.getX();
			gy = e.getY();
			Rectangle rect = new Rectangle(gx-2, gy-2, 4, 4);
			if(rect.contains(new Point(px+_dx, py+_dy))) {
				_dx = 0;
				_dy = 0;
			}
		}
	}

	/**
	 * affectation des vecteur dans les coordonnnes
	 */
	private void affectation() {
		_x += (_dx * _dzombie);
		_y += (_dy * _dzombie);
	}

	/**
	 * action du zombie attack
	 */
	@Override
	public void action() {
		for (ListIterator<Entity> i = _enemies.listIterator(); i.hasNext();) {
			Entity e = i.next();
			if (((Perso) this).distance(e, _ra)) {
				attack(e, _attackz);
				((Human)e).setInfected(true);
			}
		}
		super.action();
	}

	/**
	 * effet de l'attaque
	 */
	@Override
	void effect() {
	}
}
