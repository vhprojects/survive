package Model;

import java.util.ListIterator;

/**
 * Classe specialisée d'un humain, peut attaquer les zombies
 * @author Home
 *
 */
public class Hunter extends Human{

	public Hunter(int x, int y) {
		super(x, y);
		_attackh = 5;
		_ra = 30;
		_type = "Hunter";
	}
	
	@Override
    public void action() {
		for (ListIterator<Entity> i = _enemies.listIterator(); i.hasNext();) {
		    Entity e = i.next();
		    if (((Perso) this).distance(e, _ra)) attack(e, _attackh);
		}
		super.action();
    }
}
