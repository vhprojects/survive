package Model;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Classe qui permet la gestion des enties
 * @author Home
 *
 */
@SuppressWarnings("serial")
public class ListEntity extends ArrayList<Entity> {
	/**
	 * Attribut de ListEntity avec patterns Singleton
	 */

	public ListEntity() {
		super();
	}

	/**
	 * Si l'entité est deja dans la liste peut etre redefinir le "equals"
	 * 
	 * @param e
	 *            entité a comparer
	 * @return true si doublon, faux si pas doublons
	 */
	public boolean doublon(Entity e) {
		ListIterator<Entity> i = this.listIterator();
		boolean found = false;
		Entity temp;

		while (!found && i.hasNext()) {
			temp = i.next();
			if (Math.abs(e.getX() - temp.getX()) < 20
					&& Math.abs(e.getY() - temp.getY()) < 20)
				found = true;
		}
		return found;
	}

	@Override
	public boolean add(Entity e) {
		if (!doublon(e))
			return super.add(e);
		return false;
	};

	/**
	 * Renvoi le type de l'entité et ses coordonnées dans un string
	 */
	public String toString() {
		String s = "";
		Entity e;
		ListIterator<Entity> i = this.listIterator();
		while (i.hasNext()) {
			e = i.next();
			s += "\t" + e.getClass().getSimpleName() + "(" + e.getX() + ";"
					+ e.getY() + ")" + "\n";
		}
		return s;
	}
}
