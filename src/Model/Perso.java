package Model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ListIterator;
import java.util.Random;

import javax.swing.Timer;

/**
 * Classe de personnages qui peuvent communiquer
 * @author 
 *
 */
public abstract class Perso extends Entity {
	protected int _ra;
	protected int _rv;
	protected ListEntity _friends;
	protected ListEntity _enemies;
	protected String _type;
	protected String _typeEnemy;
	private int _dtempx = 0;
	private int _dtempy = 0;
	private Timer _randomTimer;
	private Timer _refreshListsTimer;
	private Random _randomDirection = new Random();

	protected Perso() {
	}

	public Perso(int x, int y) {
		super(x, y);
		_friends = new ListEntity();
		_enemies = new ListEntity();
		_randomTimer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_dtempx = _randomDirection.nextInt(3);
				_dtempy = _randomDirection.nextInt(3);
			}
		});
		_randomTimer.start();
		
		_refreshListsTimer = new Timer(5000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshLists();
			}
		});
		_refreshListsTimer.start();
	}

	public Timer getRandomTimer() {
		return _randomTimer;
	}

	public Timer getRefreshListsTimer() {
		return _refreshListsTimer;
	}

	public ListEntity get_friends() {
		return _friends;
	}

	public int getRA() {
		return _ra;
	}

	public int getRV() {
		return _rv;
	}

	public int getDY() {
		return _dy;
	}

	public int getDX() {
		return _dx;
	}

	public void set_friends(ListEntity _friends) {
		this._friends = _friends;
	}

	public ListEntity get_enemies() {
		return _enemies;
	}

	/**
	 * Fonction check
	 */
	@Override
	public void check() {
		// Check des ennemies et amis proches
		Perso e;
		ListIterator<Entity> i = Game.getInstance().getListEntity().listIterator();
		while (i.hasNext()) {
			e = (Perso) i.next();
			if (distance(e, _rv)) {
				// SI e est dans la classe
				String classe = getClass().getSimpleName();
				if(getType().equals("Human") || getType().equals("Hunter") || getType().equals("Medic"))
				{
					if (e.getType().equals("Human")
							|| e.getType().equals("Hunter")
							|| e.getType().equals("Medic"))
						_friends.add(e);
					else/* if (e.getClass().getSimpleName().equals(_typeEnemy))*/
						_enemies.add(e);
				}
				else if(getType().equals("Zombie")){
					if (e.getType().equals("Zombie"))
							_friends.add(e);
					else/* if (e.getClass().getSimpleName().equals(_typeEnemy))*/
						_enemies.add(e);
				}
			}
		}
	}

	/**
	 * Methode distance qui regarde si l' entité est dans le rayon On utilise la
	 * norme(infinie)
	 * 
	 * @param e
	 *            entité
	 * @param _r
	 *            rayon pour comparer
	 */
	protected boolean distance(Entity e, int r) {
		if (e.getX() == getX() && e.getY() == getY())
			return false;
		return (Math.abs(e.getX() - getX()) <= r)
				&& (Math.abs(e.getY() - getY()) <= r);
	}

	/** 
	 * Pour chaque Humain il envoit ses enemies à ses copains
	 */
	@Override
	public void speak() {
		for (ListIterator<Entity> i = _friends.listIterator(); i.hasNext();) {
			((Perso) i.next()).addEnemiesFromFriend(_enemies);
		}
	}

	/**
	 * Rajoute les enemies de l'ami (parametre implicite)
	 * @param l_e liste des enemies du copain
	 * 
	 */
	public void addEnemiesFromFriend(ListEntity l_e) {
		// insertion en fin
		ListIterator<Entity> i = l_e.listIterator();
		while (i.hasNext())
			_enemies.add(i.next());
	}

	/**
	 * Fonction generale implementer dans humain et zombie
	 */
	public void move() {
		randomMove();
	}

	/**
	 * Mouvement aleatoire du perso
	 */
	private void randomMove() {
		// on dit que c est 2,1 ou 0 et apres on fait -1 pour prevoir toutes les
		// directions
		_dx = (_dtempx - 1);
		_dy = (_dtempy - 1);
	}

	/**
	 * Fonction renvoyant le max entre i et j
	 * 
	 * @param i
	 * @param j
	 * @return renvoi la valeur max
	 */
	protected double max(int i, int j) {
		if (i < j)
			return j;
		else
			return i;
	}

	public void action() {
	}

	/**
	 * Fonction d'attaque entre appelant une fonction d'effet
	 * 
	 * @param victim
	 *            entite qui se fait attaqué
	 * @param strenghattack
	 *            force de l'attaquant
	 */
	protected void attack(Entity victim, int strenghattack) {
		victim._hp -= strenghattack;			
		((Perso) victim).effect();
	}

	/**
	 * Voir implementation dans humain et zombie
	 */
	abstract void effect();

	/**
	 * Sert au debugage
	 * 
	 * @param s
	 *            string pour afficher dans la console
	 */
	public void testAffichagePerso(String s) {
		System.out.println("****************************************\n");
		System.out.println(s + ":\n" + getClass().getSimpleName() + "("
				+ getX() + ";" + getY() + ")" + " amis :\n" + get_friends());
		System.out.println(s + ":\n" + getClass().getSimpleName() + "("
				+ getX() + ";" + getY() + ")" + " enemies :\n" + get_enemies());
		System.out.println("V:(" + _dx + " ; " + _dy + ")");
		System.out.println("hp= " + getHp());
		System.out.println("****************************************\n");
	}

	/**
	 * indique si collision avec les murs.
	 * 
	 * @return
	 */
	public void WallCollision() {
		int px = getX(), py = getY();
		int width, height;
		
		boolean one, two, three, four;
		Random r = new Random();
		one = px < 0;
		two = py < 0;
		if(this.getClass().getSimpleName().equals("Human")) width = height = Constants.getInstance().getHumanSize();
		else	width = height = Constants.getInstance().getZombieSize();
		three = px + width  > Constants.getInstance().getGAMEPANEL_SIZE()
				.getWidth();
		four = py + height > Constants.getInstance().getGAMEPANEL_SIZE()
				.getHeight();

		if (one) {
			_dx = 1;
			_dy = r.nextInt(3) - 1;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (two) {
			_dx = r.nextInt(3) - 1;
			_dy = 1;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (three) {
			_dx = -1;
			_dy = r.nextInt(3) - 1;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (four) {
			_dx = -1;
			_dy = r.nextInt(3) - 1;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (one && two) {
			_dx = 1;
			_dy = 0;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (one && four) {
			_dx = 0;
			_dy = -1;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (three && two) {
			_dx = 0;
			_dy = 1;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
		if (three && four) {
			_dx = -1;
			_dy = 0;
			_dtempx = _dx +1;
			_dtempy = _dy + 1;
		}
	}
	public void refreshLists() {
		_friends.removeAll(_friends);
		_enemies.removeAll(_enemies);
	}

	public String getType() {
		return _type;
	}
}
