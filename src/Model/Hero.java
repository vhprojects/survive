package Model;

import java.util.ListIterator;

/**
 * Classe du hero que contrle le joueur
 * @author home
 *
 */
public class Hero extends Hunter {

	public Hero(int x, int y) {
		super(x, y);
		_dx = 0;
		_dy = 0;
		_dhuman = 5;
		_rv = 60;
		_ra = 50;
		_attackh = 30;
	}
	
	@Override
	public void move() {
		_x += (_dx * _dhuman);
		_y += (_dy * _dhuman);
		
		this.effect();
	}

	/**
	 * @param x
	 * @param y
	 */
	public void move(int x, int y) {
		_dx = x;
		_dy = y;
	}
	
	@Override
	public void action() {
	}
	
	/**
	 * Attaque du héro
	 */
	public void heroAttack() {
		for (ListIterator<Entity> i = _enemies.listIterator(); i.hasNext();) {
		    Entity e = i.next();
		    if (((Perso) this).distance(e, _ra)) attack(e, _attackh);
		}
	}
}
