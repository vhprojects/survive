package Model;

import java.util.*;

import View.GUI;

/**
 * classe fait avec le design patern Singleton
 * 
 * @author Home
 * 
 */
public class Game {
	private static Game _INSTANCE = null;
	private ListEntity _listEntity;
	private boolean _finished;
	private boolean _pause;
	private boolean _init;
	private Hero _hero;

	private Game() {
		_init = false;
	}

	/**
	 * @return
	 */
	public static Game getInstance() {
		if (_INSTANCE == null)
			_INSTANCE = new Game();
		return _INSTANCE;
	}

	/**
	 * @param nb_H
	 * @param nb_Z
	 */
	public void init(int nb_H, int nb_Z) {
		if (_init == false) {
			_init = true;
			_finished = false;
			_pause = true;
			_listEntity = new ListEntity();

			/* Génération aléatoire des entités */
			int rand_x, rand_y, cpt;
			Random r = new Random();

			// Zombie
			cpt = 0;
			while (cpt < nb_Z) {
				/* Vérification des doublons */
				rand_x = r.nextInt((int) (Model.Constants.getInstance()
						.getGAMEPANEL_SIZE().getWidth() - Model.Constants
						.getInstance().getZombieSize()));
				rand_y = r.nextInt((int) (Model.Constants.getInstance()
						.getGAMEPANEL_SIZE().getHeight() - Model.Constants
						.getInstance().getZombieSize())); // 600 - Taille des
															// bonhommes

				if (_listEntity.add(new Zombie(rand_x, rand_y)))
					cpt++;
			}

			// Human
			cpt = 0;
			boolean hunter = false;
			while (cpt < nb_H) {
				/* Vérification des doublons */
				rand_x = r.nextInt((int) (Model.Constants.getInstance()
						.getGAMEPANEL_SIZE().getWidth() - Model.Constants
						.getInstance().getHumanSize()));
				rand_y = r.nextInt((int) (Model.Constants.getInstance()
						.getGAMEPANEL_SIZE().getHeight() - Model.Constants
						.getInstance().getHumanSize())); // 600 - Taille des
														// bonhommes
				//il y a une probabilité 3/10 d'avoir un chasseur 9/100 d'avoir un medic
				if(cpt==0 && hunter==false){
					_hero = new Hero(rand_x, rand_y);
					if (_listEntity.add(_hero))
						hunter = true;
				}
				
				if(r.nextInt(10)<3){
					if (_listEntity.add(new Medic(rand_x, rand_y)))
						cpt++;
				}
				else if(r.nextInt(10)<3) {
					if (_listEntity.add(new Hunter(rand_x, rand_y)))
						cpt++;
				}
				else{
					if (_listEntity.add(new Human(rand_x, rand_y)))
						cpt++;
				}
			}
		}
	}

	/**
	 * 
	 */
	public void step() {
		if (!this.isPaused()) {
			try {
				Thread.currentThread();
				Thread.sleep(20);
			} catch (InterruptedException ie) {
				// If this thread was intrrupted by another thread
			}
			
			// On relance les timers
			for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();) {
				Perso p = (Perso) i.next();
				p.getRandomTimer().start();
				p.getRefreshListsTimer().start();
			}
			this.check();
			this.speak();
			this.move();
			this.action();
		}
		else {
			// on arrête les timers
			for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();) {
				Perso p = (Perso) i.next();
				p.getRandomTimer().stop();
				p.getRefreshListsTimer().stop();
			}
		}
	}

	/**
	 * @return
	 */
	public ListEntity getListEntity() {
		return _listEntity;
	}

	/**
	 * @return
	 */
	public boolean isFinished() {
		int cptZ = 0, cptH = 0;
		for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();) {
			Entity e = i.next();
			if(((Perso)e).getType().equals("Human"))	cptH++;
			if(((Perso)e).getType().equals("Hunter"))	cptH++;
			if(((Perso)e).getType().equals("Medic"))	cptH++;
			else if (((Perso)e).getType().equals("Zombie"))	cptZ++;
		}
		if(cptZ==0 || cptH==0)
			_finished = true;
		return _finished;
	}

	/**
	 * 
	 */
	public void check() {
		// Check normal de tous les persos
		for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();)
			i.next().check();
		
		//Raffraichissement des persos si morts ou zombifiés
		ListEntity tmpList = new ListEntity();
		for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();) {
			Perso e = (Perso)i.next();
			if(e.getType().equals("Human") || e.getType().equals("Medic") || e.getType().equals("Hunter")) {
				Human h = (Human) e;
				if(e.getHp()<=0 || h.getInfection()>=350) 
				{
					// On supprime l'humain tué de toutes les listes
					for (ListIterator<Entity> j = _listEntity.listIterator(); j.hasNext();) 
					{
						Perso tmp = (Perso) j.next();
			 			tmp.refreshLists();
					}
					// On change l'humain par un zombie
					_listEntity.set(_listEntity.indexOf(e), new Zombie(e));
				}
			} else {//c est un zombie
				if(e.getHp()<=0) {
					// On supprime l'humain tué de toutes les listes
					for (ListIterator<Entity> j = _listEntity.listIterator(); j.hasNext();)  {
						Perso tmp = (Perso) j.next();
			 			tmp.refreshLists();
					}
					tmpList.add(e);
				}
			}
		}
		_listEntity.removeAll(tmpList);
		GUI.getInstance().getDebugFrame().print(_listEntity.toString());
	}
	/**
	 * 
	 */
	public void speak() {
		/* Faire les itérateurs */
		for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();)
			i.next().speak();
	}

	/**
	 * 
	 */
	public void move() {
		for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();)
			i.next().move();
	}

	/**
	 * 
	 */
	public void action() {
		for (ListIterator<Entity> i = _listEntity.listIterator(); i.hasNext();)
			i.next().action();
	}

	public boolean isPaused() {
		return _pause;
	}

	public void setPause(boolean b) {
		_pause = b;
	}

	public Hero getHero() {
		return _hero;
	}

	public boolean win() {
		ListIterator<Entity> i = _listEntity.listIterator();
		boolean Hempty=true, Zempty=true;
		Perso p = (Perso) i.next();
		return !p.getType().equals("Zombie");
	}
}
