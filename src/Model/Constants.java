package Model;

import java.awt.Color;
import java.awt.Dimension;

/**
 * Classe regroupant toutes les constantes du jeu
 * @author Home
 *
 */
public class Constants {
	private static Constants _INSTANCE = null;
	private final Dimension _GAMEPANEL_SIZE;
	private final Dimension _INTERACTPANEL_SIZE;
	private final String _ZOMBIE_TYPE;
	private final String _HUMAN_TYPE;
	private final int _HUMAN_SIZE;
	private final int _ZOMBIE_SIZE;
	private final Color _HUMAN_COLOR;
	private final Color _HUNTER_COLOR;
	private final Color _MEDIC_COLOR;
	private final Color _ZOMBIE_COLOR;

	private Constants() {
		_GAMEPANEL_SIZE = new Dimension(600, 600);
		_INTERACTPANEL_SIZE = new Dimension(200, 600);
		_ZOMBIE_TYPE = "Zombie";
		_HUMAN_TYPE = "Human";
		_ZOMBIE_SIZE = 20;
		_HUMAN_SIZE = 20;
		_HUMAN_COLOR = new Color(31, 154, 255);
		_HUNTER_COLOR = new Color(31, 154, 128);
		_ZOMBIE_COLOR = Color.RED;
		_MEDIC_COLOR = new Color(128, 128, 255);
	}

	public static Constants getInstance() {
		if (_INSTANCE == null)
			_INSTANCE = new Constants();
		return _INSTANCE;
	}

	public Dimension getGAMEPANEL_SIZE() {
		return _GAMEPANEL_SIZE;
	}

	public Dimension getINTERACTPANEL_SIZE() {
		return _INTERACTPANEL_SIZE;
	}

	public int getHumanSize() {
		return _HUMAN_SIZE;
	}

	public int getZombieSize() {
		return _ZOMBIE_SIZE;
	}

	public Color getHUMAN_COLOR() {
		return _HUMAN_COLOR;
	}
	
	public Color getHUNTER_COLOR() {
		return _HUNTER_COLOR;
	}
	public Color getZOMBIE_COLOR() {
		return _ZOMBIE_COLOR;
	}

	public Color getMEDIC_COLOR() {
		return _MEDIC_COLOR;
	}
}
