package Model;

import java.util.ListIterator;

/**
 * Classe specialisé d'humain, peut soigner ses congeneres
 * @author brice
 *
 */
public class Medic extends Human{

	public Medic(int x, int y) {
		super(x, y);
		_ra = 30;
		_type = "Medic";
	}
	
	@Override
	public void action(){
		heal();
		super.action();
	}

	private void heal() {
		Entity e;
		int hpi;
		for (ListIterator<Entity> i = _friends.listIterator(); i.hasNext();) {
			e = i.next();
			hpi = e.getHp();
			if (((Perso) this).distance(e, _ra))
				if(hpi<100) {
					e.setHp(hpi+5);
					((Human)e).setInfected(false);
				}
		}
	}
}
