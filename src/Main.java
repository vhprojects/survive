
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import sun.security.jca.GetInstance;

import Controller.Controller;
import Model.Game;

import View.GUI;
import View.GamePanel;
import View.MainFrame;

/**
 * 
 * @author home
 * Classe Main centrale
 */
public class Main {
	static public void main(String args[]) {
		Controller controller = new Controller(true);
		Game game = Game.getInstance();
		GUI gui = GUI.getInstance();

		/* Boucle de jeu */
		while (!game.isFinished()) {
			game.step();
			gui.update();
		}
		JFrame fin;
		fin = new JFrame();
		fin.setResizable(false);
		fin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fin.pack();
		fin.setFocusable(true);
		fin.setVisible(true);
		fin.setBounds(100, 100, 200, 100);
		

		JButton b = new JButton("OK");
		b.setBounds(new Rectangle(100,100));
		b.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				System.exit(0);
			}
		});
		if(game.win()){
			fin.setTitle("Partie Gagnée");
			fin.add(b);
		}
		else{
			fin.setTitle("Partie Perdue");
			fin.add(b);
		}
	}
}
