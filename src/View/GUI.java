package View;

import Model.Game;

/**
 * Classe construite sous le model Singleton
 * Permet de faire initialiser la MainFrame et la debugFrame
 * @author home
 *
 */
public class GUI {
	private boolean _init;
	private static GUI _INSTANCE = null;
	private Game _game = null;
	private MainFrame _mainFrame;
	private DebugFrame _debugFrame;

	private GUI() {
		_init = false;
	}

	public static GUI getInstance() {
		if (_INSTANCE == null)
			_INSTANCE = new GUI();
		return _INSTANCE;
	}

	public void init(int nb_H, int nb_Z) {
		if (_init == false) {
			_init = true;
			_game = Game.getInstance();
			_game.init(nb_H, nb_Z);
			_mainFrame = new MainFrame();
		}
	}
	
	public void initDebugMode() {
		_debugFrame = new DebugFrame();
	}

	public MainFrame getMainFrame() {
		return _mainFrame;
	}

	public DebugFrame getDebugFrame() {
		return _debugFrame;
	}

	public void update() {
		_mainFrame.draw();
	}
}
