package View;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

import Model.Entity;

/**
 * Classe faisant reference a la class Entity
 * Dessine les entites correspondante
 * @author Home
 *
 */
public abstract class EntityGUI {
	protected int _size;
	protected Color _color;
	protected Rectangle2D _rect;
	protected Entity _entity;

	public EntityGUI(Entity entity) {
		_entity = entity;
	}

	public int getX() {
		return _entity.getX();
	}

	public int getY() {
		return _entity.getY();
	}

	public int getSize() {
		return _size;
	}

	public Color getColor() {
		return _color;
	}

	public Rectangle2D getRect() {
		return _rect;
	}
}
