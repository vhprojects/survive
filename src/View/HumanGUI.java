package View;

import java.awt.geom.Rectangle2D;

import Model.Entity;

/**
 * Classe faisant reference a la classe humain
 * Donne les caracteristiques visuelle a un humain
 * @author Home
 * 
 */
public class HumanGUI extends PersoGUI {
	/**
	 * @param entity
	 */
	public HumanGUI(Entity entity) {
		super(entity);
		_color = Model.Constants.getInstance().getHUMAN_COLOR();
		_size = Model.Constants.getInstance().getHumanSize();
		_rect = new Rectangle2D.Double(entity.getX(), entity.getY(), _size,
				_size);
	}
}
