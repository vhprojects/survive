package View;

public interface Drawable {
	public void draw();
}
