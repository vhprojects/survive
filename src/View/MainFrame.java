package View;

import javax.swing.JFrame;

import Model.Constants;

/**
 * Classe faisant reference a la classe gamePanel
 *
 * @author Home
 * 
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame implements Drawable {
	private GamePanel _gamePanel;
	private InteractPanel _interactPanel;

	/**
	 * 
	 */
	public MainFrame() {
		super();
		_gamePanel = new GamePanel(Constants.getInstance().getGAMEPANEL_SIZE());
		// _interactPanel = new InteractPanel();

		this.add(_gamePanel);
		// this.add(_interactPanel);

		/* Propriétés fenêtre */
		this.setResizable(false);
		this.setTitle("Survive !!");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.toFront();
		this.setVisible(true);
	}

	public GamePanel getGamePanel() {
		return _gamePanel;
	}

	public InteractPanel getInteractPanel() {
		return _interactPanel;
	}

	@Override
	public void draw() {
		_gamePanel.repaint();
		// _interactPanel.draw();
	}
}
