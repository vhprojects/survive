package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.*;

import Model.Entity;
import Model.Game;

/**
 * Classe faisant reference au panneauxde jeu
 * Dessine les personnages
 * @author home
 * 
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel {
	private ArrayList<PersoGUI> _listEntity;

	/**
	 * @param dimension
	 * 
	 */
	public GamePanel(Dimension dimension) {
		super();
		this.setPreferredSize(dimension);
		this.setFocusable(true);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		// Dessin des entité_
		_listEntity = new ArrayList<PersoGUI>();
		for (ListIterator<Entity> i = Game.getInstance().getListEntity()
				.listIterator(); i.hasNext();) {
			Entity e = i.next();
			if (e.getClass().getSimpleName().equals("Human"))
				_listEntity.add(new HumanGUI(e));
			else if (e.getClass().getSimpleName().equals("Zombie"))
				_listEntity.add(new ZombieGUI(e));
			else if (e.getClass().getSimpleName().equals("Hunter") || e.getClass().getSimpleName().equals("Hero"))
				_listEntity.add(new HunterGUI(e));
			else if (e.getClass().getSimpleName().equals("Medic"))
				_listEntity.add(new MedicGUI(e));
			// else
			// _listEntity.add(new EntityGUI(i.current()));
		}
		
		for (int i = 0; i < _listEntity.size(); i++) {
			g2.setColor(_listEntity.get(i).getColor());
			g2.fill(_listEntity.get(i).getRect());
			g2.draw(_listEntity.get(i).getRect());
			g2.draw(new Rectangle2D.Double(_listEntity.get(i).getX()
					- _listEntity.get(i).getRA(), _listEntity.get(i).getY()
					- _listEntity.get(i).getRA(),
					_listEntity.get(i).getRA() * 2 + 1, _listEntity.get(i)
							.getRA() * 2 + 1));

			g2.setColor(Color.BLACK);
			g2.draw(new Rectangle2D.Double(_listEntity.get(i).getX()
					- _listEntity.get(i).getRV(), _listEntity.get(i).getY()
					- _listEntity.get(i).getRV(),
					_listEntity.get(i).getRV() * 2 + 1, _listEntity.get(i)
							.getRV() * 2 + 1));
		}
	}
}
