package View;

import Model.Entity;
import Model.Perso;

/**
 * Classe faisant reference a la classe perso
 * Donne les caracteristiques visuelle a un Perso
 * @author home
 *
 */
public abstract class PersoGUI extends EntityGUI {

	public PersoGUI(Entity entity) {
		super(entity);
	}

	public int getRA() {
		return ((Perso) _entity).getRA();
	}

	public int getRV() {
		return ((Perso) _entity).getRV();
	}

}
