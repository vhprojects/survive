package View;

import java.awt.geom.Rectangle2D;

import Model.Entity;

/**
 * Classe faisant reference a la classe medic
 * Donne les caracteristiques visuelle a un medic
 * @author home
 *
 */
public class MedicGUI extends HumanGUI {
 
	public MedicGUI(Entity entity) {
		super(entity);
		_color = Model.Constants.getInstance().getMEDIC_COLOR();
		_size = Model.Constants.getInstance().getHumanSize();
		_rect = new Rectangle2D.Double(entity.getX(), entity.getY(), _size,_size);
	}

}
