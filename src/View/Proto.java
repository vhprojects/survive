package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

@SuppressWarnings("serial")
public class Proto extends JFrame {
	private JPanel _gamePanel;
	private JPanel _skillPanel;
	private JPanel _infoPanel;

	public Proto() {
		super();
		this.setTitle("Survive !!");

		_gamePanel = new JPanel();
		_skillPanel = new JPanel();
		_infoPanel = new JPanel();

		_gamePanel.setBackground(Color.RED);
		_skillPanel.setBackground(Color.CYAN);
		_infoPanel.setBackground(Color.YELLOW);

		_gamePanel.setPreferredSize(new Dimension(600, 600));
		_skillPanel.setPreferredSize(new Dimension(200, 400));
		_infoPanel.setPreferredSize(new Dimension(100, 200));

		_skillPanel.setLayout(new BoxLayout(_skillPanel, BoxLayout.Y_AXIS));
		_skillPanel.add(new JLabel("SKILL"));
		_skillPanel.add(new JLabel(" "));
		_skillPanel.add(new JLabel("Soldier"));
		_skillPanel.add(new JLabel("Hunter"));
		_skillPanel.add(new JLabel("Sniper"));
		_skillPanel.add(new JLabel("Medic"));

		JProgressBar bar = new JProgressBar(0, 100);
		bar.setStringPainted(true);
		_infoPanel.add(bar);
		_infoPanel.add(new JLabel("HP"));

		JPanel interactPanel = new JPanel(new BorderLayout());
		interactPanel.add(_skillPanel, BorderLayout.NORTH);
		interactPanel.add(_infoPanel, BorderLayout.SOUTH);

		this.setLayout(new BorderLayout());
		this.add(_gamePanel, BorderLayout.CENTER);
		this.add(interactPanel, BorderLayout.EAST);

		this.pack();

		this.getContentPane().setPreferredSize(new Dimension(800, 600));
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
