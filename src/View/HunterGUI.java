package View;

import java.awt.geom.Rectangle2D;

import Model.Entity;

/**
 * Classe faisant reference a la classe humain
 * Donne les caracteristiques visuelle a un humain
 * @author Home
 * 
 */
public class HunterGUI extends HumanGUI {
	/**
	 * @param entity
	 */
	public HunterGUI(Entity entity) {
		super(entity);
		_color = Model.Constants.getInstance().getHUNTER_COLOR();
		_size = Model.Constants.getInstance().getHumanSize();
		_rect = new Rectangle2D.Double(entity.getX(), entity.getY(), _size,
				_size);
	}
}
