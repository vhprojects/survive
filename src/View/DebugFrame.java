package View;

import java.util.ListIterator;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import Model.Entity;
import Model.Human;
import Model.Perso;

/**
 * Classe pour afficher la fenetre de debeugage
 * @author Home
 *
 */
@SuppressWarnings("serial")
public class DebugFrame extends JFrame {
	private JTextArea _textArea;

	public DebugFrame() {
		_textArea = new JTextArea();
		this.add(_textArea);
		this.setLocation(900, 600);
		this.setTitle("DebugFrame");
		this.setSize(400, 400);
		this.setVisible(true);
	}

	public void print(String s) {
		_textArea.removeAll();
		_textArea.setText(s);
	}

	public String getEntityInfo(Entity e) {
		String s = "-------------------------------------\n";
		s += e.getClass().getSimpleName() + "\tHP : " + e.getHp() + (e.getClass().getSimpleName().equals("Human") || e.getClass().getSimpleName().equals("Hunter")|| e.getClass().getSimpleName().equals("Hero") ? "\tInfection : " + ((Human)e).isInfected() + ((Human)e).getInfection() + "\n" : "\n");
		s += "Position (" + e.getX() + ", " + e.getY() + ")\n";
		s += "Vitesse  (" + ((Perso) e).getDX() + ", " + ((Perso) e).getDY()
				+ ")\n";
		s += "Liste ami :\n";
		for (ListIterator<Entity> i = ((Perso) e).get_friends().listIterator(); i
				.hasNext();) {
			Entity tmp = i.next();
			s += "\t" + tmp.getClass().getSimpleName() + " (" + tmp.getX()
					+ ", " + tmp.getY() + ")\n";
		}
		s += "\nListe ennemies :\n";
		for (ListIterator<Entity> i = ((Perso) e).get_enemies().listIterator(); i
				.hasNext();) {
			Entity tmp = i.next();
			s += "\t" + tmp.getClass().getSimpleName() + " (" + tmp.getX()
					+ ", " + tmp.getY() + ")\n";
		}
		s += "\n-------------------------------------";
		return s;
	}
}
