package View;

import java.awt.geom.Rectangle2D;

import Model.Entity;

/**
 * Classe faisant reference a la classe zombie
 * Donne les caracteristiques visuelle a un zombie
 * @author Home
 * 
 */
public class ZombieGUI extends PersoGUI {
	/**
	 * @param entity
	 */
	public ZombieGUI(Entity entity) {
		super(entity);
		_color = Model.Constants.getInstance().getZOMBIE_COLOR();
		_size = Model.Constants.getInstance().getZombieSize();
		_rect = new Rectangle2D.Double(entity.getX(), entity.getY(), _size,
				_size);
	}
}
