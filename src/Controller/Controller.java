package Controller;

import java.awt.*;
import java.awt.event.*;
import java.util.ListIterator;

import javax.swing.SwingUtilities;

import Model.*;
import View.*;

/**
 * @author Home
 * 
 */
public class Controller {
	private GUI _gui;
	private Game _game;

	public Controller(boolean b) {
		this();
		if(b)	_gui.initDebugMode();
	}
	
	public Controller() {
		// RELEASE
		// BeginFrame bf = new BeginFrame();
		// while(!bf.isFinished()) {}
		// bf.dispose();
		// bf = null;
		// _game = Game.getInstance();
		// _game.init(bf.getNbZombie, bf.getNbHuman);
		// RELEASE
		_game = Game.getInstance();
		_gui = GUI.getInstance();
		_gui.init(5, 1);
		_gui.getMainFrame().getGamePanel().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// DEBUG
				Point p = arg0.getPoint();
				for (ListIterator<Entity> i = _game.getListEntity()
						.listIterator(); i.hasNext();) {
					Entity e = i.next();
					Rectangle r = new Rectangle(e.getX() - 8, e.getY() - 8, 16, 16);
					if (r.contains(p)) {
						_gui.getDebugFrame().print(
								_gui.getDebugFrame().getEntityInfo(e));
					}
				}
				// DEBUG
				
				
				// Attaquer
				_game.getHero().heroAttack();
				
				// Mettre le jeu en pause
				if (SwingUtilities.isRightMouseButton(arg0)) {
					_game.setPause(!_game.isPaused());
				}
			}
		});
		
		_gui.getMainFrame().getGamePanel().addKeyListener(new KeyAdapter() {			
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyChar()=='q' || e.getKeyChar()=='Q')
					_game.getHero().setDx(0);
				
				if(e.getKeyChar()=='d' || e.getKeyChar()=='D')
					_game.getHero().setDx(0);
				
				if(e.getKeyChar()=='s' || e.getKeyChar()=='S')
					_game.getHero().setDy(0);
				
				if(e.getKeyChar()=='z' || e.getKeyChar()=='Z')
					_game.getHero().setDy(0);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				boolean left, right, up, down;
				
				left = e.getKeyChar()=='q' || e.getKeyChar()=='Q';
				right = e.getKeyChar()=='d' || e.getKeyChar()=='D';
				down = e.getKeyChar()=='s' || e.getKeyChar()=='S';
				up = e.getKeyChar()=='z' || e.getKeyChar()=='Z';
				
				if(left) _game.getHero().setDx(-1);
				
				if(right) _game.getHero().setDx(1);
				
				if(down) _game.getHero().setDy(1);
				
				if(up) _game.getHero().setDy(-1);
			}
		});
	}
}
