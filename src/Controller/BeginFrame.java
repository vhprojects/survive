package Controller;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * 
 * @author 
 * Classe de configuration de la fenetre
 */
@SuppressWarnings("serial")
public class BeginFrame extends JFrame {
	private boolean _finish;
	private int _nbZombie;
	private int _nbHuman;
	
	/**
	 * 
	 * @author home
	 *	classe pour connaitre le type des personnages
	 */
	class BeginActionListener implements ActionListener {
		private String _nbZombieString;
		private String _nbHumanString;

		public BeginActionListener(JTextField nbZombie, JTextField nbHuman) {
			_nbZombieString = nbZombie.getText();
			_nbHumanString = nbHuman.getText();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				_nbZombie = Integer.parseInt(_nbZombieString);
			} catch (NumberFormatException e2) {
				_nbZombie = 5;
			}
			try {
				_nbHuman = Integer.parseInt(_nbHumanString);
			} catch (NumberFormatException e1) {
				_nbHuman = 5;
			}
			_finish = true;
		}
	}

	public BeginFrame() {
		_finish = false;
		this.setTitle("Begin");
		this.setLayout(new BorderLayout());
		this.add(new JLabel("How many Zombies and Human ?"), BorderLayout.NORTH);

		JPanel fields = new JPanel();
		fields.add(new JLabel("Zombies :"));
		fields.add(new JLabel("Humans :"));
		JTextField nbZombieField = new JTextField(2);
		JTextField nbHumanField = new JTextField(2);
		fields.add(nbZombieField);
		fields.add(nbHumanField);
		this.add(fields, BorderLayout.CENTER);

		JButton validate = new JButton("Let's go !!");
		BeginActionListener bal = new BeginActionListener(nbZombieField,
				nbHumanField);
		validate.addActionListener(bal);

		this.add(validate, BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
	}

	public boolean isFinished() {
		System.out.println(_finish);
		return _finish;
	}

	public int getNbZombie() {
		return _nbZombie;
	}

	public int getNbHuman() {
		return _nbHuman;
	}
}
